import {Component} from '@angular/core';

declare const require: any;

const videojs = require('./lib/videojs-hls/video.js');

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';
  player;

  prepare(video) {
    this.player = videojs(video, {}, function onPlayerReady() {
      console.log('Your player is ready!');


      // How about an event listener?
      this.on('ended', function() {
        console.log('Awww...over so soon?!');
      });
    });
    video.poster = 'http://10.42.0.1/videos/mp4/thumb.png';
    this.player.src('http://10.42.0.1/videos/mp4/TV%20UFAM%20Play-pY5piYZmdkM.mp4');
  }

  play() {
    this.player.play();
  }
}
